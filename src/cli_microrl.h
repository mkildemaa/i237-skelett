#ifndef CLI_MICRORL_H
#define CLI_MICRORL_H


typedef struct card {
    char *UID;
    char *size;
    char *name;
    struct card *next;
} card0;

int cli_execute(int argc, const char *const *argv);
extern card0 *head;

#endif

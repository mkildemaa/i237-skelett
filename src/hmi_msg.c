#include <avr/pgmspace.h>

const char m1[] PROGMEM = "null";
const char m2[] PROGMEM = "yks";
const char m3[] PROGMEM = "kaks";
const char m4[] PROGMEM = "kolm";
const char m5[] PROGMEM = "neli";
const char m6[] PROGMEM = "viis";
const char m7[] PROGMEM = "kuus";
const char m8[] PROGMEM = "seitse";
const char m9[] PROGMEM = "kaheksa";
const char m10[] PROGMEM = "üheksa";

PGM_P const wnumbers[] PROGMEM = {m1, m2, m3, m4, m5, m6, m7, m8, m9, m10};

#include <time.h>
#include <string.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "hmi_msg.h"
#include "print_helper.h"
#include "cli_microrl.h"
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "../lib/andy_brown_memdebug/memdebug.h"
#define BLINK_DELAY_MS 100
#define UART_BAUD 9600
#define UART_STATUS_MASK 0x00FF

typedef enum {
    door_opening,
    door_open,
    door_closing,
    door_closed
} door_state_t;

door_state_t door_state = door_closed;
volatile uint32_t system_time;

static microrl_t rl;
static microrl_t *prl = &rl;

static inline void init_rfid_reader(void)
{
    /* Init RFID-RC522 */
    MFRC522_init();
    PCD_Init();
}

static inline void init_leds(void)
{
    DDRA |= _BV(DDA0);
    DDRA |= _BV(DDA2);
    DDRA |= _BV(DDA4);
    DDRB |= _BV(DDB7);
    PORTB &= ~_BV(PORTB7);
}

static inline void init_micro(void)
{
    microrl_init (prl, uart0_puts);
    microrl_set_execute_callback (prl, cli_execute);
}




static inline void show_lcd(void)
{
    lcd_puts_P(PSTR(LCD_NAME));
    lcd_goto(LCD_ROW_2_START);
    lcd_puts_P(PSTR("Uks lukus"));
}

static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if (prev_time != now) {
        ltoa(now, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        PORTA ^= _BV(PORTA2);
        prev_time = now;
    }
}
static inline void init_sys_timer(void)
{
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12);
    TCCR1B |= _BV(CS12);
    OCR1A = 62549;
    TIMSK1 |= _BV(OCIE1A);
}

static inline void show_errcon(void)
{
    uart1_puts_p(PSTR(VER_FW));
    uart1_puts_p(PSTR(VER_LIBC));
}

static inline uint32_t current_time(void)
{
    uint32_t current_time;
    ATOMIC_BLOCK(ATOMIC_FORCEON) {
        current_time = system_time;
    }
    return current_time;
}


static inline void door_status()
{
    switch (door_state) {
    case door_opening:
        DDRA ^= _BV(DDA2);
        PORTA |= _BV(PORTA0);
        door_state = door_open;
        break;

    case door_open:
        break;

    case door_closing:
        door_state = door_closed;
        PORTA &= ~_BV(PORTA0);
        DDRA ^= _BV(DDA2);
        break;

    case door_closed:
        break;
    }
}


static inline void rfid_scanner(void)
{
    Uid uid;
    Uid *uid_ptr = &uid;
    bool status;
    bool used_error;
    static bool message_status;
    static bool door_open_status;
    uint32_t time_current = current_time();
    static uint32_t message_start;
    static uint32_t door_open_start;
    static uint32_t read_start;
    static char *used_name;
    char *uidName;
    byte as0[10];
    byte as1[10];

    if ((read_start + 1) < time_current) {
        if (PICC_IsNewCardPresent()) {
            read_start = time_current;
            PICC_ReadCardSerial(&uid);
            uidName = binTohex(uid_ptr->uidByte, uid_ptr->size);
            card0 *checker = head;

            while (checker != NULL) {
                if (strcmp(checker->UID, uidName) == 0) {
                    status = true;
                    break;
                }

                status = false;
                checker = checker->next;
            }

            if (status) {
                if (checker->name != used_name || used_name == NULL) {
                    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
                    lcd_goto(LCD_ROW_2_START);
                    lcd_puts(checker->name);
                    used_name = checker->name;
                    used_error = false;
                }

                if (door_state != door_open) {
                    door_state = door_opening;
                    door_open_status = true;
                }

                door_open_start = time_current;
            } else {
                if (!used_error) {
                    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
                    lcd_goto(LCD_ROW_2_START);
                    lcd_puts_P(PSTR("Tundmatu kaart"));
                    used_error = true;
                    used_name = NULL;
                }

                if (door_state != door_closed) {
                    door_state = door_closing;
                    door_open_status = false;
                }
            }

            free(uidName);
            message_status = true;
            message_start = time_current;
            PICC_WakeupA(as0, as1);
        }
    }

    if ((message_start + 5) < time_current && message_status) {
        lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
        lcd_goto(LCD_ROW_2_START);
        lcd_puts_P(PSTR("Uks lukus"));
        used_error = false;
        used_name = NULL;
        message_status = false;
    }

    if ((door_open_start + 3) < time_current && door_open_status) {
        door_state = door_closing;
        door_open_status = false;
    }

    door_status(door_state);
}

static inline void init_hw(void)
{
    DDRD |= _BV(DDD3);
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    init_sys_timer();
    sei();
    lcd_init();
    lcd_clrscr();
}
void main(void)
{
    init_leds();
    init_hw();
    init_micro();
    show_errcon();
    uart0_puts_p(PSTR(ST_NAME));
    show_lcd();
    init_rfid_reader();

    while (1) {
        heartbeat();
        rfid_scanner();
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
    }
}


ISR(TIMER1_COMPA_vect)
{
    system_tick();
    system_time++;
}
//

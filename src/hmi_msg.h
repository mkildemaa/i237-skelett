#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_
#define VER_FW "Version: "FW_VERSION" built on: "__DATE__" "__TIME__"\r\n"
#define VER_LIBC "avr-libc version: "__AVR_LIBC_VERSION_STRING__" avr-gcc version: "__VERSION__"\r\n"
#define ST_NAME "Markus Kildemaa \r\n"
#define LCD_NAME "Markus Kildemaa"
#define C_NUMBER "Sisestasid numbri: "
#define W_NUMBER "Palun sisesta number vahemikus 0 ja 9!\r\n"
#define S_NUMBER "Viga!"

extern PGM_P const wnumbers[];

#endif

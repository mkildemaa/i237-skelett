#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/andy_brown_memdebug/memdebug.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "hmi_msg.h"
#include "cli_microrl.h"
#include "print_helper.h"

#define NUM_ELEMS(x) (sizeof(x) / sizeof((x)[0]))

void cli_print_help(const char *const *argv);
void cli_example(const char *const *argv);
void cli_print_ver(const char *const *argv);
void cli_print_ascii_tbls(const char *const *argv);
void cli_handle_number(const char *const *argv);
void cli_rfid_read(const char *const *argv);
void cli_rfid_add(const char *const *argv);
void cli_rfid_list(const char *const *argv);
void cli_rfid_delete(const char *const *argv);
void cli_mem_stat(const char *const *argv);

card0 *head = NULL;

void cli_print_cmd_error(void);
void cli_print_cmd_arg_error(void);

typedef struct cli_cmd {
    PGM_P cmd;
    PGM_P help;
    void (*func_p)();
    const uint8_t func_argc;
} cli_cmd_t;

const char help_cmd[] PROGMEM = "help";
const char help_help[] PROGMEM = "Get help";
const char example_cmd[] PROGMEM = "example";
const char example_help[] PROGMEM =
    "Prints out all provided 3 arguments Usage: example <argument> <argument> <argument>";
const char ver_cmd[] PROGMEM = "version";
const char ver_help[] PROGMEM = "Print FW version";
const char ascii_cmd[] PROGMEM = "ascii";
const char ascii_help[] PROGMEM = "Print ASCII tables";
const char number_cmd[] PROGMEM = "number";
const char number_help[] PROGMEM =
    "Print and display matching number Usage: number <decimal number>";

const char rfid_read_cmd[] PROGMEM = "read";
const char rfid_read_text[] PROGMEM = "Loe kaart";
const char rfid_add_cmd[] PROGMEM = "add";
const char rfid_add_text[] PROGMEM = "Lisa kaarte nimekirja: add <UID> <Name>";
const char rfid_list_cmd[] PROGMEM = "list";
const char rfid_list_text[] PROGMEM = "Näita nimekirja kaartidest";
const char rfid_delete_cmd[] PROGMEM = "delete";
const char rfid_delete_text[] PROGMEM =
    "Kustuta kaart nimekirjast: delete <UID>";
const char mem_stat_cmd[] PROGMEM = "mem";
const char mem_stat_text[] PROGMEM =
    "Näita mälukasutust ning võrdle eelnevaga";


const cli_cmd_t cli_cmds[] = {
    {help_cmd, help_help, cli_print_help, 0},
    {ver_cmd, ver_help, cli_print_ver, 0},
    {example_cmd, example_help, cli_example, 3},
    {ascii_cmd, ascii_help, cli_print_ascii_tbls, 0},
    {number_cmd, number_help, cli_handle_number, 1},
    {rfid_read_cmd, rfid_read_text, cli_rfid_read, 0},
    {rfid_add_cmd, rfid_add_text, cli_rfid_add, 2},
    {rfid_list_cmd, rfid_list_text, cli_rfid_list, 0},
    {rfid_delete_cmd, rfid_delete_text, cli_rfid_delete, 1},
    {mem_stat_cmd, mem_stat_text, cli_mem_stat, 0}
};



void cli_rfid_read(const char *const *argv)
{
    (void) argv;
    Uid uid;
    Uid *uid_ptr = &uid;
    byte as0[10];
    byte as1[10];

    if (PICC_IsNewCardPresent()) {
        uart0_puts_p(PSTR("\n Kaart selekteeritud! \n"));
        PICC_ReadCardSerial(uid_ptr);
        uart0_puts_p(PSTR("\n UID: \n"));
        uart0_puts(binTohex(uid_ptr->uidByte, uid_ptr->size));
        uart0_puts_p(PSTR("\n UID sak: \n"));
        uart0_puts(PICC_GetTypeName(PICC_GetType(uid_ptr->sak)));
        uart0_puts_p(PSTR("\n UID pikkus: \n"));
        uart0_puts(binTohex(&uid.size, sizeof uid_ptr->size));
        PICC_WakeupA(as0, as1);
    } else {
        uart0_puts_p(PSTR("\n Unable to select card! \n"));
    }
}

void cli_rfid_add(const char *const *argv)
{
    (void) *argv;
    char *uidName;
    uint8_t uidSizeInt;
    char *uidSize;
    char *name;
    uidName = malloc(strlen(argv[1]) + 1);
    strcpy(uidName, argv[1]);
    uidSizeInt = strlen(uidName) / 2;

    if (uidSizeInt > 10) {
        uart0_puts_p(PSTR("\n UID pikkus liiga suur (10 baiti MAX)! \n"));
        return;
    }

    card0 *checker = head;

    while (checker != NULL) {
        if (strcmp(checker->UID, uidName) == 0) {
            uart0_puts_p(PSTR("\n See kaart nimekirjas juba olemas! \n"));
            free(uidName);
            return;
        }

        checker = checker->next;
    }

    card0 *newCard = (malloc(sizeof(card0)));

    if (newCard == NULL) {
        uart0_puts_p(PSTR("\n Mälu nimekirjas ebaõnnestus! \n"));
        free (newCard);
        free (uidName);
        return;
    }

    name = malloc(strlen(argv[2]) + 1);
    strcpy(name, argv[2]);
    uidSize = malloc(uidSizeInt + 1);
    uidSize = itoa(uidSizeInt, uidSize, 10);
    newCard->UID = uidName;
    newCard->size = uidSize;
    newCard->name = name;
    newCard->next = NULL;

    if (head == NULL) {
        uart0_puts_p(PSTR("\n Kaardi lisamine õnnestus! \n"));
    } else {
        card0 *temp = head;

        while (temp->next != NULL) {
            temp = temp->next;
        }

        uart0_puts_p(PSTR("\n Kaardi lisamine õnnestus! \n"));
    }
}

void cli_rfid_list(const char *const *argv)
{
    (void) argv;
    card0 *current = head;
    int number = 1;
    char* numberChar = NULL;

    if (head == NULL) {
        uart0_puts_p(PSTR("\n Nimekiri on tühi \n"));
    } else {
        while (current != NULL) {
            uart0_puts_p(PSTR("\n Number: "));
            uart0_puts(itoa(number, numberChar, 10));
            uart0_puts_p(PSTR("\n UID: "));
            uart0_puts(current->UID);
            uart0_puts_p(PSTR("\n UID pikkus: "));
            uart0_puts(current->size);
            uart0_puts_p(PSTR("\n Nimi: "));
            uart0_puts(current->name);
            number++;
            current = current->next;
        }
    }
}

void cli_rfid_delete(const char *const *argv)
{
    (void) argv;

    if (head == NULL) {
        uart0_puts_p(PSTR("\n Nimekiri on tühi \n"));
    } else {
        card0 *current = head;
        card0 *previous = head;
        card0 *temp_card = NULL;
        char *uidName = malloc(strlen(argv[1]) + 1);
        strcpy(uidName, argv[1]);

        if (strcmp(current->UID, uidName) == 0) {
            temp_card = head->next;
            free(head);
            head = temp_card;
            uart0_puts_p(PSTR("\n Kaart kustutati nimekirjast edukalt! \n"));
            return;
        }

        while (current != NULL) {
            if (strcmp(current->UID, uidName) == 0) {
                card0 *temp_card = current;
                previous->next = current->next;
                free(temp_card);
                uart0_puts_p(PSTR("\n Kaart kustutati nimekirjast edukalt! \n"));
                return;
            }

            previous = current;
            current = current->next;
        }

        uart0_puts_p(PSTR("\n Seda kaarti pole nimekirjas! \n"));
    }
}


void cli_mem_stat(const char *const *argv)
{
    (void) argv;
    char print_buf[256] = {0x00};
    extern int __heap_start, *__brkval;
    int v;
    int space;
    static int prev_space;
    space = (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
    uart0_puts_p(PSTR("Heap statistics\r\n"));
    sprintf_P(print_buf, PSTR("Used: %u\r\nFree: %u\r\n"), getMemoryUsed(),
              getFreeMemory());
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nSpace between stack and heap:\r\n"));
    sprintf_P(print_buf, PSTR("Current  %d\r\nPrevious %d\r\nChange   %d\r\n"),
              space, prev_space, space - prev_space);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nFreelist\r\n"));
    sprintf_P(print_buf, PSTR("Freelist size:             %u\r\n"),
              getFreeListSize());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Blocks in freelist:        %u\r\n"),
              getNumberOfBlocksInFreeList());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest block in freelist: %u\r\n"),
              getLargestBlockInFreeList());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest freelist block:    %u\r\n"),
              getLargestAvailableMemoryBlock());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest allocable block:   %u\r\n"),
              getLargestNonFreeListBlock());
    uart0_puts(print_buf);
    prev_space = space;
}


void cli_print_help(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR("Implemented commands:\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        uart0_puts_p(cli_cmds[i].cmd);
        uart0_puts_p(PSTR(" : "));
        uart0_puts_p(cli_cmds[i].help);
        uart0_puts_p(PSTR("\n"));
    }
}

void cli_example(const char *const *argv)
{
    uart0_puts_p(PSTR("Command had following arguments:\n"));

    for (uint8_t i = 1; i < 4; i++) {
        uart0_puts(argv[i]);
        uart0_puts_p(PSTR("\n"));
    }
}

void cli_print_ver(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR(VER_FW));
    uart0_puts_p(PSTR(VER_LIBC));
}

void cli_print_ascii_tbls(const char *const *argv)
{
    (void) argv;
    print_ascii_tbl();
    unsigned char ascii[128] = {0};

    for (unsigned char i = 0; i < sizeof(ascii); i++) {
        ascii[i] = i;
    }

    print_for_human(ascii, sizeof(ascii));
}

void cli_handle_number(const char *const *argv)
{
    (void) argv;
    int number = atoi(argv[1]);

    for (size_t i = 0; i < strlen(argv[1]); i++) {
        if (!isdigit(argv[1][i])) {
            uart0_puts_p(PSTR("Argument is not a decimal number!\n"));
            lcd_clrscr();
            lcd_puts_P(PSTR(S_NUMBER));
            return;
        }
    }

    if (number >= 0 && number <= 9) {
        uart0_puts_p(PSTR(C_NUMBER));
        uart0_puts_p((PGM_P)pgm_read_word(&(wnumbers[number])));
        uart0_puts_p(PSTR("\n"));
        lcd_clrscr();
        lcd_puts_P((PGM_P)pgm_read_word(&(wnumbers[number])));
    } else {
        uart0_puts_p(PSTR(W_NUMBER));
        lcd_clrscr();
        lcd_puts_P(PSTR(S_NUMBER));
    }
}

void cli_print_cmd_error(void)
{
    uart0_puts_p(PSTR("Command not implemented. \n Use <help> to get help. \n"));
}

void cli_print_cmd_arg_error(void)
{
    uart0_puts_p(
        PSTR("To few or too many arguments for this command \n Use <help> \n"));
}

int cli_execute(int argc, const char *const *argv)
{
    // Move cursor to new line. Then user can see what was entered.
    uart0_puts_p(PSTR("\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        if (!strcmp_P(argv[0], cli_cmds[i].cmd)) {
            // Test do we have correct arguments to run command
            // Function arguments count shall be defined in struct
            if ((argc - 1) != cli_cmds[i].func_argc) {
                cli_print_cmd_arg_error();
                return 0;
            }

            // Hand argv over to function via function pointer,
            // cross fingers and hope that funcion handles it properly
            cli_cmds[i].func_p (argv);
            return 0;
        }
    }

    cli_print_cmd_error();
    return 0;
}
